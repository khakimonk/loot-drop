import SwadeNPCSheet from "../../../systems/swade/module/sheets/SwadeNPCSheet.js";
import SwadeItem from "../../../systems/swade/module/entities/SwadeItem.js";

class LootSheetSwade extends SwadeNPCSheet {
  get template() {
    if (!game.user.isGM && this.actor.limited) {
      return 'systems/swade/templates/actors/limited-sheet.html';
    } else {
      return 'modules/loot-drop/templates/loot-sheet.html';
    } 
  }
  static get defaultOptions() {
    const options = super.defaultOptions;

    mergeObject(options, {
        classes: ["swade sheet actor npc npc-sheet loot-sheet-npc"],
        width: 890,
        height: 750
      });
    return options;
  }
}

Actors.registerSheet("swade", LootSheetSwade, {
  types: ["npc"],
  makeDefault: false
});